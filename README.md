## Description
Excel sheet project which written in pure php

## Functional requirement
>save sheets

>multiple sheet

>add reach text to sheet

>operations on cells

>possibility to link to diffrent cell from diffrent sheet 

>add filters to search in cells

>autocomplete 

## Techniques && Tools
* mvc
* php(7.2)
* mysql(5.7)
* jquery
* bootstrap
* design pattern
    * singleton

## DEMO
To watch demo check this link please [demo](https://drive.google.com/open?id=121iu03cjjUvUuzJPje507YpdKAWVW5bk)